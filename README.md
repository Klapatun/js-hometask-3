# hometask-10

## Ответьте на вопросы

#### 1. Как можно сменить контекст вызова? Перечислите все способы и их отличия друг от друга.
> Ответ: При помощи bind, call и apply. В bind привязка контекста осуществляется заранее, перед вызовом самой функции (ранее связывание), а в call/apply привязка происходит непосредственно перед вызовом функции. Call от apply отличается способом передачи аргумента: в call они передаются через запятую, а в apply в качестве массива  
#### 2. Что такое стрелочная функция?
> Ответ: Более короткий вид записи функции, который к тому же у данной функции нет this и контекст она берет из внешнего лексического окружения.
#### 3. Приведите свой пример конструктора. 
```js
// Ответ:

function MyConstructor(whose, why, whatDoing) {
  this.whose = whose;
  this.why = why;
  this.whatDoing = whatDoing;
}

let x = new MyConstructor('Мой','Потому что','Ничего');

```
#### 4. Исправьте код так, чтобы в `this` попадал нужный контекст. Исправить нужно 3мя способами, как показано в примерах урока.

```js
  const person = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person.sayHello();

  const person1 = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(() => {
          console.log(this.name + ' says hello to everyone!');
      }, 1000)
    }
  }

  person1.sayHello();

  const person2 = {
    name: 'Nikita',
    sayHello: function() {
      setTimeout(function() {
          console.log(this.name + ' says hello to everyone!')
      }.bind(this), 1000)
    }
  }

  person2.sayHello();

const person3 = {
  name: 'Nikita',
  sayHello: function() {
    setTimeout.call(this,function() {
        console.log(this.name + ' says hello to everyone!')
    }.bind(this), 1000)
  }
}

person3.sayHello();

```

## Выполните задания

* Установите зависимости `npm install`;
* Допишите функции в `task.js`;
* Проверяйте себя при помощи тестов `npm run test`;
* Создайте Merge Request с решением.
